package junit;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import ers.dao.UsersDao;
import ers.dao.UsersDaoImpl;
import ers.model.Users;


public class JUnit {

	UsersDao myUser = new UsersDaoImpl();
	
	
	@Test
	public void passwordTest() {
		
		String pass = "password";
		
		Users currentUser = myUser.selectUserByUsername("daBoss");
		
		
		assertEquals(pass, currentUser.getPassword(), "testing to see if selects the appropriate user");

	}
	
	
}
