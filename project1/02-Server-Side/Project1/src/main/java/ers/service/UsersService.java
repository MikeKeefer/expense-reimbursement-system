package ers.service;

import java.util.List;

import ers.model.Users;

public interface UsersService {

	//Insert (create)
//	public boolean addUser(Users myUser);
	
	//Selects (read)
	public Users findUserByUsername(String userName);
	public List<Users> findAllUsers();
//	public Users findUserByEmail(String email);
	
	
//	//Updates (update)
//	public boolean amendUsername(Users myUser);
//	public boolean amendPassword(Users myUsers);
//	public boolean amendFirstName(Users myUsers);
//	public boolean amendLastName(Users myUsers);
//	public boolean amendEmail(Users myUsers);
//	public boolean amendUserRole(Users myUsers);
//	
//	//Delete (delete)
//	public boolean removeUser(Users myUsers);
}
