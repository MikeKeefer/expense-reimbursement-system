package ers.service;

import java.util.List;

import ers.model.Reimbursements;

public interface ReimbursementService {

	//Insert (Create)
	public boolean addReimbursement(Reimbursements myReimbursement);
	
	//Select (Read)
	public List<Reimbursements> findAllReimbursements();
	public List<Reimbursements> findAllReimbursementsByAuthorId(int Id);
	public List<Reimbursements> findAllReimbursementsByStatus(String myStatus);
	public Reimbursements findReimbursementById(Reimbursements myReimbursement);
	
	//Update
	public boolean amendReimbursementStatus(Reimbursements myReimbursement); //Change Status
}
