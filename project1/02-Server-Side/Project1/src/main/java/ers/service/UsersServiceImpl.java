package ers.service;

import java.util.List;

import ers.dao.UsersDao;
import ers.dao.UsersDaoImpl;
import ers.model.Users;

public class UsersServiceImpl implements UsersService {

	UsersDao myDao = new UsersDaoImpl();
	
//	@Override
//	public boolean addUser(Users myUser) {
//		return myDao.insertUser(myUser);
//	}

	@Override
	public Users findUserByUsername(String userName) {
		return myDao.selectUserByUsername(userName);
	}


	@Override
	public List<Users> findAllUsers() {
		return myDao.selectAllUsers();
	}

//	@Override
//	public Users findUserByEmail(String email) {
//		return myDao.selectUserByEmail(email);
//	}
	
//	@Override
//	public boolean amendUsername(Users myUser) {
//		return myDao.updateUsername(myUser);
//	}
//
//	@Override
//	public boolean amendPassword(Users myUsers) {
//		return myDao.updatePassword(myUsers);
//	}
//
//	@Override
//	public boolean amendFirstName(Users myUsers) {
//		return myDao.updateFirstName(myUsers);
//	}
//
//	@Override
//	public boolean amendLastName(Users myUsers) {
//		return myDao.updateLastName(myUsers);
//	}
//
//	@Override
//	public boolean amendEmail(Users myUsers) {
//		return myDao.updateEmail(myUsers);
//	}
//
//	@Override
//	public boolean amendUserRole(Users myUsers) {
//		return myDao.updateUserRole(myUsers);
//	}
//
//	@Override
//	public boolean removeUser(Users myUsers) {
//		return myDao.deleteUser(myUsers);
//	}

}
