package ers.service;

import java.util.List;

import ers.dao.ReimbursementsDao;
import ers.dao.ReimbursementsDaoImpl;
import ers.model.Reimbursements;

public class ReimbursementServiceImpl implements ReimbursementService {

	ReimbursementsDao myDao = new ReimbursementsDaoImpl();
	
	@Override
	public boolean addReimbursement(Reimbursements myReimbursement) {
		return myDao.insertReimbursement(myReimbursement);
	}

	@Override
	public List<Reimbursements> findAllReimbursements() {
		return myDao.selectAllReimbursements();
	}
	
	@Override
	public List<Reimbursements> findAllReimbursementsByAuthorId(int Id) {
		return myDao.selectAllReimbursementsByAuthorId(Id);
	}

	@Override
	public List<Reimbursements> findAllReimbursementsByStatus(String myStatus) {
		return myDao.selectAllReimbursementsByStatus(myStatus);
	}

	@Override
	public Reimbursements findReimbursementById(Reimbursements myReimbursement) {
		return myDao.selectReimbursementById(myReimbursement);
	}

	@Override
	public boolean amendReimbursementStatus(Reimbursements myReimbursement) {
		return myDao.updateReimbursementStatus(myReimbursement);
	}

}
