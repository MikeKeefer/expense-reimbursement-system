package ers.model;



/**
 * Class to pass strings into Jackson
 * @author mtkee
 *
 */
public class MyCustomMessages {

	private String firstMessage;
	private String secondMessage;
	public MyCustomMessages() {
		super();
	}
	public MyCustomMessages(String firstMessage, String secondMessage) {
		super();
		this.firstMessage = firstMessage;
		this.secondMessage = secondMessage;
	}
	public String getFirstMessage() {
		return firstMessage;
	}
	public void setFirstMessage(String firstMessage) {
		this.firstMessage = firstMessage;
	}
	public String getSecondMessage() {
		return secondMessage;
	}
	public void setSecondMessage(String secondMessage) {
		this.secondMessage = secondMessage;
	}
	@Override
	public String toString() {
		return "MyCustomMessages [firstMessage=" + firstMessage + ", secondMessage=" + secondMessage + "]";
	}
	
	
	
}
