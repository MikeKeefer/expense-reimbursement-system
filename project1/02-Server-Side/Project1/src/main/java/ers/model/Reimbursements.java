package ers.model;

import java.sql.Timestamp;
import java.util.Objects;

public class Reimbursements {
	//STATES
	int reimbId;
	double amount;
	Timestamp submitted;
	Timestamp resolved;
	String description;
	String receipt;
	int authorId;
	String author;
	int resolverId;
	String resolver;
	int statusId;
	String status;
	int typeId;
	String type;
	
	//GETTERS and SETTERS
	public int getReimbId() {
		return reimbId;
	}
	public void setReimbId(int reimbId) {
		this.reimbId = reimbId;
	}
	
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public Timestamp getSubmitted() {
		return submitted;
	}
	public void setSubmitted(Timestamp submitted) {
		this.submitted = submitted;
	}
	public Timestamp getResolved() {
		return resolved;
	}
	public void setResolved(Timestamp resolved) {
		this.resolved = resolved;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getReceipt() {
		return receipt;
	}
	public void setReceipt(String receipt) {
		this.receipt = receipt;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getResolver() {
		return resolver;
	}
	public void setResolver(String resolver) {
		this.resolver = resolver;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	public int getAuthorId() {
		return authorId;
	}
	public void setAuthorId(int authorId) {
		this.authorId = authorId;
	}
	public int getResolverId() {
		return resolverId;
	}
	public void setResolverId(int resolverId) {
		this.resolverId = resolverId;
	}
	public int getStatusId() {
		return statusId;
	}
	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}
	public int getTypeId() {
		return typeId;
	}
	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}
	
	//CONSTRUCTORS
	
	public Reimbursements() {
		super();
	}
	
	public Reimbursements(int reimbId, double amount, Timestamp submitted, Timestamp resolved, String description,
			String receipt, int authorId, String author, int resolverId, String resolver, int statusId, String status,
			int typeId, String type) {
		super();
		this.reimbId = reimbId;
		this.amount = amount;
		this.submitted = submitted;
		this.resolved = resolved;
		this.description = description;
		this.receipt = receipt;
		this.authorId = authorId;
		this.author = author;
		this.resolverId = resolverId;
		this.resolver = resolver;
		this.statusId = statusId;
		this.status = status;
		this.typeId = typeId;
		this.type = type;
	}
	
	public Reimbursements(int reimbId, double amount, Timestamp submitted, String description, String receipt,
			int authorId, String author, int statusId, String status, int typeId, String type) {
		super();
		this.reimbId = reimbId;
		this.amount = amount;
		this.submitted = submitted;
		this.description = description;
		this.receipt = receipt;
		this.authorId = authorId;
		this.author = author;
		this.statusId = statusId;
		this.status = status;
		this.typeId = typeId;
		this.type = type;
	}
	
	
	//TO STRING
	@Override
	public String toString() {
		return "Reimbursements [reimbId=" + reimbId + ", amount=" + amount + ", submitted=" + submitted + ", resolved="
				+ resolved + ", description=" + description + ", receipt=" + receipt + ", author=" + author
				+ ", resolver=" + resolver + ", status=" + status + ", type=" + type + "]";
	}
	@Override
	public int hashCode() {
		return Objects.hash(amount, author, authorId, description, receipt, reimbId, resolved, resolver, resolverId,
				status, statusId, submitted, type, typeId);
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Reimbursements other = (Reimbursements) obj;
		return Double.doubleToLongBits(amount) == Double.doubleToLongBits(other.amount)
				&& Objects.equals(author, other.author) && authorId == other.authorId
				&& Objects.equals(description, other.description) && Objects.equals(receipt, other.receipt)
				&& reimbId == other.reimbId && Objects.equals(resolved, other.resolved)
				&& Objects.equals(resolver, other.resolver) && resolverId == other.resolverId
				&& Objects.equals(status, other.status) && statusId == other.statusId
				&& Objects.equals(submitted, other.submitted) && Objects.equals(type, other.type)
				&& typeId == other.typeId;
	}
	
	
	
	
	
	
	
	
	
	

}
