package ers.dao;

import java.util.List;

import ers.model.Users;

public interface UsersDao {

	/*
	 * Commented Lines are not part of MVP
	 * They are intended for future functionality
	 */
	//Insert (create)
//	public boolean insertUser(Users myUser);
	
	//Selects (read)
	public Users selectUserByUsername(String userName);
	public List<Users> selectAllUsers();
//	public Users selectUserByEmail(String email);
	
	
	//Updates (update)
//	public boolean updateUsername(Users myUser);
//	public boolean updatePassword(Users myUsers);
//	public boolean updateFirstName(Users myUsers);
//	public boolean updateLastName(Users myUsers);
//	public boolean updateEmail(Users myUsers);
//	public boolean updateUserRole(Users myUsers);
	
	//Delete (delete)
//	public boolean deleteUser(Users myUsers);


	
	
}
