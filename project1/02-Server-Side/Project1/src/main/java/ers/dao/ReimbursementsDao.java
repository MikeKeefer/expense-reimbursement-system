package ers.dao;

import java.util.List;

import ers.model.Reimbursements;

public interface ReimbursementsDao {
	
	//Insert (Create)
	public boolean insertReimbursement(Reimbursements myReimbursement);
	
	//Select (Read)
	public List<Reimbursements> selectAllReimbursements();
	public List<Reimbursements> selectAllReimbursementsByAuthorId(int Id);
	public List<Reimbursements> selectAllReimbursementsByStatus(String myStatus);
	public Reimbursements selectReimbursementById(Reimbursements myReimbursement);
	
	/*
	 * Commented out for future use
	 */
//	public Reimbursements selectReimbursementByUsername(Reimbursements myReimbursement);
//	public Reimbursements selectReimbursementByAuthor(Reimbursements myReimbursement);
//	public Reimbursements selectReimbursementByResolver(Reimbursements myReimbursement);
//	public Reimbursements selectReimbursementByStatus(Reimbursements myReimbursement);
//	public Reimbursements selectReimbursementByType(Reimbursements myReimbursement);
//	public Reimbursements selectReimbursementByDateSubmitted(Reimbursements myReimbursement);
//	public Reimbursements selectReimbursementByDateResolved(Reimbursements myReimbursement);
	
	//Update
	public boolean updateReimbursementStatus(Reimbursements myReimbursement); //Change Status
//	public boolean updateReimbursementResolver(Reimbursements myReimbursement); //Assign Resolver
//	public boolean updateReimbursementResolveDate(Reimbursements myReimbursement); //Add timestamp to resolver
	
	
	
	//Delete
//	public boolean deleteReimbursement(Reimbursements myReimbursement);
	

}
