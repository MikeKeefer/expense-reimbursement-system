package ers.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import ers.model.Reimbursements;

public class ReimbursementsDaoImpl implements ReimbursementsDao {
	final static Logger loggy = Logger.getLogger(ReimbursementsDaoImpl.class);

	@Override
	public boolean insertReimbursement(Reimbursements myReimbursement) {
		try(Connection conn = ConnectionGenerator.getConnection()) {
			String sql = "INSERT INTO ers_reimbursement ("
					+ "reimb_amount, reimb_submitted"
					+ ", reimb_description, reimb_reciept, reimb_author "
					+ ", reimb_status_id, reimb_type_id)"
					+ " VALUES (?,current_timestamp,?,?,?,?,?);";
			
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setDouble(1, myReimbursement.getAmount());
			ps.setString(2, myReimbursement.getDescription());
			ps.setString(3, myReimbursement.getReceipt());
			ps.setInt(4, myReimbursement.getAuthorId());
			ps.setInt(5, myReimbursement.getStatusId());
			ps.setInt(6, myReimbursement.getTypeId());
			
			
			ps.executeUpdate();
			
			
		}catch(SQLException e) {
			loggy.error(e);
			return false;
		}
		return true;
	}

	@Override
	public List<Reimbursements> selectAllReimbursements() {
	List<Reimbursements> reimbList = new ArrayList<>();
		
		try(Connection conn = ConnectionGenerator.getConnection())
		{
			//Perform selection in SQL
			String sql = "SELECT * FROM reimb_table_view; ";
			
			PreparedStatement ps = conn.prepareStatement(sql);
			
			ResultSet rs = ps.executeQuery();
			
			Reimbursements myReimb = new Reimbursements();
			while(rs.next()) {
				myReimb = new Reimbursements(rs.getInt("reimb_id")
						, rs.getDouble("reimb_amount")
						, rs.getTimestamp("reimb_submitted")
						, rs.getTimestamp("reimb_resolved")
						, rs.getString("reimb_description")
						, rs.getString("reimb_reciept")
						, rs.getInt("reimb_author")
						, rs.getString("author_firstname")+" "+rs.getString("author_lastname")
						, rs.getInt("reimb_resolver")
						, rs.getString("resolver_firstname")+" "+rs.getString("resolver_lastname")
						, rs.getInt("reimb_status_id")
						, rs.getString("current_status")
						, rs.getInt("reimb_type_id")
						, rs.getString("expense_type")
						);
				reimbList.add(myReimb);
			}
		}catch(SQLException e) {
			loggy.error(e);
		}
		return reimbList;
	}
	@Override
	public List<Reimbursements> selectAllReimbursementsByAuthorId(int Id) {
		List<Reimbursements> reimbList = new ArrayList<>();
		
		try(Connection conn = ConnectionGenerator.getConnection())
		{
			//Perform selection in SQL
			String sql = "SELECT * FROM reimb_table_view WHERE reimb_author = ?; ";
			
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, Id);
			
			
			ResultSet rs = ps.executeQuery();
			
			Reimbursements myReimb = new Reimbursements();
			while(rs.next()) {
				myReimb = new Reimbursements(rs.getInt("reimb_id")
						, rs.getDouble("reimb_amount")
						, rs.getTimestamp("reimb_submitted")
						, rs.getTimestamp("reimb_resolved")
						, rs.getString("reimb_description")
						, rs.getString("reimb_reciept")
						, rs.getInt("reimb_author")
						, rs.getString("author_firstname")+" "+rs.getString("author_lastname")
						, rs.getInt("reimb_resolver")
						, rs.getString("resolver_firstname")+" "+rs.getString("resolver_lastname")
						, rs.getInt("reimb_status_id")
						, rs.getString("current_status")
						, rs.getInt("reimb_type_id")
						, rs.getString("expense_type")
						);
				reimbList.add(myReimb);
			}
		}catch(SQLException e) {
			loggy.error(e);
		}
		return reimbList;
	}

	@Override
	public List<Reimbursements> selectAllReimbursementsByStatus(String myStatus) {
	List<Reimbursements> reimbList = new ArrayList<>();
		
		try(Connection conn = ConnectionGenerator.getConnection())
		{
			//Perform selection in SQL
			String sql = "SELECT * FROM reimb_table_view WHERE current_status = ? ; ";
			
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, myStatus);
			
			
			ResultSet rs = ps.executeQuery();
			
			Reimbursements myReimb = new Reimbursements();
			while(rs.next()) {
				myReimb = new Reimbursements(rs.getInt("reimb_id")
						, rs.getDouble("reimb_amount")
						, rs.getTimestamp("reimb_submitted")
						, rs.getTimestamp("reimb_resolved")
						, rs.getString("reimb_description")
						, rs.getString("reimb_reciept")
						, rs.getInt("reimb_author")
						, rs.getString("author_firstname")+" "+rs.getString("author_lastname")
						, rs.getInt("reimb_resolver")
						, rs.getString("resolver_firstname")+" "+rs.getString("resolver_lastname")
						, rs.getInt("reimb_status_id")
						, rs.getString("current_status")
						, rs.getInt("reimb_type_id")
						, rs.getString("expense_type")
						);
				reimbList.add(myReimb);
			}
		}catch(SQLException e) {
			loggy.error(e);
		}
		return reimbList;
	}

	@Override
	public Reimbursements selectReimbursementById(Reimbursements myReimbursement) {
		Reimbursements targetReimb = null;
		
		try(Connection conn = ConnectionGenerator.getConnection())
		{
			//Perform selection in SQL
			String sql = "SELECT * FROM reimb_table_view WHERE reimb_id =?";
			
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, myReimbursement.getReimbId());
			
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				targetReimb = new Reimbursements(rs.getInt("reimb_id")
						, rs.getDouble("reimb_amount")
						, rs.getTimestamp("reimb_submitted")
						, rs.getTimestamp("reimb_resolved")
						, rs.getString("reimb_description")
						, rs.getString("reimb_reciept")
						, rs.getInt("reimb_author")
						, rs.getString("author_firstname")+" "+rs.getString("author_lastname")
						, rs.getInt("reimb_resolver")
						, rs.getString("resolver_firstname")+" "+rs.getString("resolver_lastname")
						, rs.getInt("reimb_status_id")
						, rs.getString("current_status")
						, rs.getInt("reimb_type_id")
						, rs.getString("expense_type")
						);
			}
		}catch(SQLException e) {
			loggy.error(e);
		}
		return targetReimb;
	}

	@Override
	public boolean updateReimbursementStatus(Reimbursements myReimbursement) {
		try(Connection conn = ConnectionGenerator.getConnection()) {
			String sql = "UPDATE ers_reimbursement SET "
					+ "reimb_resolved = current_timestamp "
					+ ", reimb_resolver = ? "
					+ ", reimb_status_id = ?"
					+ " WHERE "
					+ "reimb_id = ? ;";
			
			PreparedStatement ps = conn.prepareStatement(sql);
//			ps.setTimestamp(1, myReimbursement.getResolved());
			ps.setInt(1, myReimbursement.getResolverId());
			ps.setInt(2, myReimbursement.getStatusId());
			ps.setInt(3, myReimbursement.getReimbId());
			
//			System.out.println(ps);
			
			ps.executeUpdate();
			
		}catch(SQLException e) {
			loggy.error(e);
			return false;
		}
		return true;
	}

}
