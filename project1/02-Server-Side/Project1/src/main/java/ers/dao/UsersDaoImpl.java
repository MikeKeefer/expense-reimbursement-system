package ers.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import ers.model.Users;

public class UsersDaoImpl implements UsersDao {
	final static Logger loggy = Logger.getLogger(UsersDaoImpl.class);

//	@Override
//	public boolean insertUser(Users myUser) {
//		try(Connection conn = ConnectionGenerator.getConnection()) {
//			String sql = "INSERT INTO ers_users ("
//					+ "user_name, user_password, user_first_name"
//					+ ", user_last_name, user_email, user_role_id)"
//					+ " VALUES (?,?,?,?,?,?);";
//			
//			PreparedStatement ps = conn.prepareStatement(sql);
//			ps.setString(1, myUser.getUsername());
//			ps.setString(2, myUser.getPassword());
//			ps.setString(3, myUser.getFirstName());
//			ps.setString(4, myUser.getLastName());
//			ps.setString(5, myUser.getEmail());
//			ps.setInt(6, myUser.getRoleId());
//			
//			ps.executeUpdate();
//			
//		}catch(SQLException e) {
//			loggy.error(e);
//			return false;
//		}
//		return true;
//	}

	@Override
	public Users selectUserByUsername(String userName) {
		Users targetUser = null;
		
		try(Connection conn = ConnectionGenerator.getConnection())
		{
			//Perform selection in SQL
			String sql = "SELECT * FROM user_table_view WHERE ers_username =?";
			
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, userName);
			
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				targetUser = new Users(rs.getInt("ers_users_id")
						, rs.getString("ers_username")
						, rs.getString("ers_password")
						, rs.getString("user_first_name")
						, rs.getString("user_last_name")
						, rs.getString("user_email")
						, rs.getString("user_role")
						);
			}
		}catch(SQLException e) {
			loggy.error(e);
		}
		return targetUser;
	}

	@Override
	public List<Users> selectAllUsers() {
		List<Users> userList = new ArrayList<>();
		
		try(Connection conn = ConnectionGenerator.getConnection())
		{
			//Perform selection in SQL
			String sql = "SELECT * FROM user_table_view; ";
			
			PreparedStatement ps = conn.prepareStatement(sql);
			
			ResultSet rs = ps.executeQuery();
			
			Users currentUser = new Users();
			while(rs.next()) {
				currentUser = new Users(rs.getInt("ers_users_id")
						, rs.getString("ers_username")
						, rs.getString("ers_password")
						, rs.getString("user_first_name")
						, rs.getString("user_last_name")
						, rs.getString("user_email")
						, rs.getString("user_role")
						);
				userList.add(currentUser);
//				System.out.println(rs.getString("user_role"));
			}
		}catch(SQLException e) {
			loggy.error(e);
		}
		return userList;
	}

//	@Override
//	public Users selectUserByEmail(String myEmail) {
//		Users targetUser = null;
//		
//		try(Connection conn = ConnectionGenerator.getConnection())
//		{
//			//Perform selection in SQL
//			String sql = "SELECT * FROM user_table_view WHERE user_email =?";
//			
//			PreparedStatement ps = conn.prepareStatement(sql);
//			ps.setString(1, myEmail);
//			
//			ResultSet rs = ps.executeQuery();
//			
//			while(rs.next()) {
//				targetUser = new Users(rs.getInt("ers_users_id")
//						, rs.getString("ers_username")
//						, rs.getString("ers_password")
//						, rs.getString("user_first_name")
//						, rs.getString("user_last_name")
//						, rs.getString("user_email")
//						, rs.getInt("user_role_id")
//						);
//				System.out.println(rs.getString("user_role"));
//			}
//		}catch(SQLException e) {
//			loggy.error(e);
//		}
//		return targetUser;
//	}
//	
//	@Override
//	public boolean updateUsername(Users myUser) {
//		try(Connection conn = ConnectionGenerator.getConnection()) {
//			String sql = "UPDATE ers_users SET "
//					+ "user_name = ? "
//					+ "WHERE "
//					+ "ers_users_id = ? ;";
//			
//			PreparedStatement ps = conn.prepareStatement(sql);
//			ps.setString(1, myUser.getUsername());
//			ps.setInt(2, myUser.getUserId());
//			
//			ps.executeUpdate();
//			
//		}catch(SQLException e) {
//			loggy.error(e);
//			return false;
//		}
//		return true;
//	}
//
//	@Override
//	public boolean updatePassword(Users myUser) {
//		// TODO Auto-generated method stub
//		return false;
//	}
//
//	@Override
//	public boolean updateFirstName(Users myUser) {
//		// TODO Auto-generated method stub
//		return false;
//	}
//
//	@Override
//	public boolean updateLastName(Users myUser) {
//		// TODO Auto-generated method stub
//		return false;
//	}
//
//	@Override
//	public boolean updateEmail(Users myUser) {
//		// TODO Auto-generated method stub
//		return false;
//	}
//
//	@Override
//	public boolean updateUserRole(Users myUser) {
//		// TODO Auto-generated method stub
//		return false;
//	}
//
//	@Override
//	public boolean deleteUser(Users myUser) {
//		// TODO Auto-generated method stub
//		return false;
//	}
//


}
