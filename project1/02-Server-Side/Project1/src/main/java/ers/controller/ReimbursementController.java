package ers.controller;


import org.apache.log4j.Logger;

import ers.model.Reimbursements;
import ers.service.ReimbursementService;
import ers.service.ReimbursementServiceImpl;
import io.javalin.http.Context;

public class ReimbursementController {

	//log4j
	public static Logger loggy = Logger.getLogger(ReimbursementController.class);

	//GET REIMBURSEMENT LIST FROM DATABASE
	static ReimbursementService myReimbursementService = new ReimbursementServiceImpl();
	
	
	//CONSTRUCTOR
	public ReimbursementController() {
		
	}
	
	
	//HANDLER LOGIC
	public static void getAllReimbursements(Context context) {
		if(context.sessionAttribute("currentUser")!=null) {
			if(context.sessionAttribute("userRole").equals("Finance Manager")) {
				context.json(myReimbursementService.findAllReimbursements());
			}else {
				context.json(myReimbursementService.findAllReimbursementsByAuthorId(
						context.sessionAttribute("currentUserId")));
			}
		}else {
			context.json(new Reimbursements());
		}
	}
	
	public static void createReimbursement(Context context) {
		Reimbursements myNewReimb = context.bodyAsClass(Reimbursements.class);
		myNewReimb.setAuthorId(context.sessionAttribute("userId"));
		//set status to pending
		myNewReimb.setStatusId(1);
		
//		System.out.println(myNewReimb);
		
		boolean result = myReimbursementService.addReimbursement(myNewReimb);
		if (result)
			context.result("Success");
		else {
			context.result("Failure");
			loggy.error("Failed to add Reimbursement");
		}
	}
	public static void updateReimbursement(Context context) {
		Reimbursements myNewReimb = context.bodyAsClass(Reimbursements.class);
		myNewReimb.setResolverId(context.sessionAttribute("userId"));
		
//		System.out.println(myNewReimb);
		
		boolean result = myReimbursementService.amendReimbursementStatus(myNewReimb);
		if (result)
			context.result("Success");
		else {
			context.result("Failure");
			loggy.error("Failed to amend Reimbursement");
		}
		
	}
	
	
	
}
