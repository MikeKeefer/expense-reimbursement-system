package ers.controller;

import java.util.List;

import org.apache.log4j.Logger;

import ers.model.Users;
import ers.service.UsersService;
import ers.service.UsersServiceImpl;
import io.javalin.http.Context;

public class UserController {

	//log4j
	public static Logger loggy = Logger.getLogger(UserController.class);

	//GET USERS LIST FROM DATABASE
	static UsersServiceImpl myUserService = new UsersServiceImpl();
	private static List<Users> myUsers = myUserService.findAllUsers();
	
	
	//CONSTRUCTOR
	public UserController() {
		
	}
	
	/////HANDLER LOGIC
	public static void logout(Context context) {
		context.sessionAttribute("currentUser", null);
//		context.redirect("/index.html");
	}
	
	public static void login(Context context) {
		
//		context.redirect("/html/homepage.html");
		String username = context.formParam("myUsername");
		String password = context.formParam("myPassword");
		loggy.info("User login attempt: "+username);
//		System.out.println(username+" "+password);

		boolean successfulLogin = false;
		for(Users temp: myUsers) {
//			System.out.println(temp.getUsername());
			if((temp.getUsername().equals(username) && temp.getPassword().equals(password))) {
				successfulLogin = true;
				context.sessionAttribute("currentUser", temp);
				context.sessionAttribute("currentUserId", temp.getUserId());
				context.sessionAttribute("userRole", temp.getRole());
				context.sessionAttribute("userId", temp.getUserId());
			}
		}
		
		if(successfulLogin) {
			loggy.info("Successfull login: "+username);
			switch(context.sessionAttribute("userRole").toString()) {
			
			case "admin":
			case "Employee":
				context.redirect("/html/employee.html");
				break;
			case "Finance Manager":
				context.redirect("/html/manager.html");
				break;
			default:
				context.redirect("/html/badlogin.html");
				loggy.warn("Invalid Role Assignment");
				break;
			}
			context.result("successful login");
		}else {
			context.redirect("/html/badlogin.html");
			loggy.warn("Invalid Login attempt");
		}
	}
	
	public static void getUserByUsername(Context context) {
		if(context.sessionAttribute("currentUser")!=null) {
			context.json(context.sessionAttribute("currentUser"));
		}else {
			context.json(new Users());
		}
	}
	
	public static void getAllUsers(Context context) {
		context.json(myUsers);
	}
	
	
}
