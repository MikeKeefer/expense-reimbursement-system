package ers;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/*
 * LOGGING LEVELS INCULDE
 * 
 * (lowest priority)				(highest priority)
 * ALL < debug < info < warn < error < fatal < off
 * 
 */

public class LogDriver {

	final static Logger loggy = Logger.getLogger(LogDriver.class);
	
	public static void main(String[] args) {
		loggy.setLevel(Level.ALL);
	
	}

}
