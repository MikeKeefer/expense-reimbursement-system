package ers.frontcontroller;

import static io.javalin.apibuilder.ApiBuilder.get;
import static io.javalin.apibuilder.ApiBuilder.path;
import static io.javalin.apibuilder.ApiBuilder.post;

import ers.controller.ReimbursementController;
import ers.controller.UserController;
import io.javalin.Javalin;

public class Dispatcher {

	///////////CONSTRUCTOR
	public Dispatcher(Javalin app) {
		setupAllPaths(app);
	}
	
	public static void setupAllPaths(Javalin app) {
		setupUserControllerPaths(app);
		setupReimbursementControllerPaths(app);
	}
	
	public static void setupUserControllerPaths(Javalin app) {
		app.routes(()->{
			path("/api/user", ()->{
				get(UserController::getAllUsers);
				path("/login",()->{
					post(UserController::login); //endpoint: /api/user/login
					get(UserController::getUserByUsername);	
				});
				path("/logout", ()->{
					post(UserController::logout);
				});
			});
			
		});
	}
	
	public static void setupReimbursementControllerPaths(Javalin app) {
		app.routes(()->{
			path("/api/reimbursements", ()->{
				get(ReimbursementController::getAllReimbursements);
				path("/createreimbursement",()->{
					post(ReimbursementController::createReimbursement);
				});
				path("/amendreimbursement",()->{
					post(ReimbursementController::updateReimbursement);
				});
			});
		});
			
	}
	
	
}
