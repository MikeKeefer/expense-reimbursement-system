package ers.frontcontroller;

import io.javalin.Javalin;
import io.javalin.http.Context;

/**
 * Class to service all possible endpoints
 * 
 * @author mtkee
 *
 */
public class FrontController {
	
	Javalin app;
	Dispatcher dispatcher;
	
	//CONSTRUCTOR
	public FrontController(Javalin app) {
		this.app=app;
		
		//Send to middleware before request/response reaches endpoint
		//create logic for each individual path
		app.before("/api/*", FrontController::checkAllRequests);
		app.before("/html/*", FrontController::checkAllRequests);
		
		
		this.dispatcher = new Dispatcher(app);
	}

	///////MIDDLEWARE LOGIC
	/**
	 * Method to check access levels for each endpoint
	 * @param context
	 */
	public static void checkAllRequests(Context context) {
		//just checks if they are logged in
		if (context.path().equals("/api/user/login")||context.path().equals("/html/badlogin.html"))
			return;
		if(context.sessionAttribute("currentUserId")==null) {
			context.redirect("/index.html");
		}else {return;}
	}
	
	public static void checkAdmin(Context context){
		if(!context.sessionAttribute("userRole").equals("admin")) {
			context.status(403);
		}
		return;
	}

	public static void clearSession(Context context) {

	}
	
	
	
	
}
