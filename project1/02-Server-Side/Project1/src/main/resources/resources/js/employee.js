window.onload = function(){
	getUser(); //go get the CURRENT user's information FROM THE CURRENT SESSION
    getMyReimbursements();
    
    document
    	.getElementById("createReimb")
    	.addEventListener('click', addReimbursement);
    	
    document
    	.getElementById("myFilter")
    	.addEventListener('change', filterTable);
    	
 	document
    	.getElementById("logout")
    	.addEventListener('click', logout);
    	
}

function logout(){
	let xhttp = new XMLHttpRequest();
	xhttp.open('POST', 'http://localhost:9001/api/user/logout');
	xhttp.send();
}


async function getUser(){

    const responsePayload = await fetch(`http://localhost:9001/api/user/login`);
    const ourJSON = await responsePayload.json();
    
	console.log(ourJSON);

    userDOMManipulation(ourJSON);
}

async function getMyReimbursements(){
	
	const responsePayload = await fetch(`http://localhost:9001/api/reimbursements`);
    const ourJSON = await responsePayload.json();
    
	console.log(ourJSON);

    tableDOMManipulation(ourJSON);
	
}

async function addReimbursement(){
	//perform an xhttp request 
	//Get ID's and values for each input/ form input
	let jsonObj=
	{
		"description": document.getElementById("description").value,
		"amount" : document.getElementById("amount").value,
		"typeId" : document.getElementById("typeId").value,
		"receipt" : document.getElementById("receipt").value,
	}
		
	
	let xhttp = new XMLHttpRequest();
	
	xhttp.onreadystatechange = function(){
		
		if(xhttp.readyState==4 && xhttp.status==200){
			
			console.log(xhttp.responseText);
			window.location.replace(location.href);
		}
	}
	xhttp.open('POST', `/api/reimbursements/createreimbursement`); //open endpoint
	
	xhttp.send(JSON.stringify(jsonObj));
	
}

function filterTable(){
	let selection, table, tr, td, txtValue;
	selection = document.getElementById("myFilter").value;
	table = document.getElementById("myReimbTable");
	tr = table.getElementsByTagName("tr");
	
	for(let row of tr){
		td=row.getElementsByTagName("td")[3];
		if(td){
			txtValue = td.textContent||td.innerText;
			if(txtValue.indexOf(selection)>-1||selection=="unfiltered"){
				row.style.display="";
			}else{
				row.style.display="none";
			}
		}
	}
}



//DOM to identify USER in heading
function userDOMManipulation(ourObjectFromJSON){
    document.getElementById("userHeader").innerText = `Welcome, ${ourObjectFromJSON.firstName} ${ourObjectFromJSON.lastName}`;
}

//DOM to modify table to view USERs Reimbursement
function tableDOMManipulation(ourObjectFromJSON){
	let tableRef = document.getElementById("myReimbTable");
	tableRef.style.visibility="visible";
	
	for(let currentReimb of ourObjectFromJSON){
		//STEP 1: create new element
		let newTR = document.createElement("tr");
		let idTH = document.createElement("th");
		
		
		let amountTD = document.createElement("td");
		let typeTD = document.createElement("td");
		let descrTD = document.createElement("td");
		let statusTD = document.createElement("td");
		let submitDateTD = document.createElement("td");
		let authorTD = document.createElement("td");
		let resolveDateTD = document.createElement("td");
		let resolverTD = document.createElement("td");
		let receiptTD = document.createElement("td")
		
		//STEP2: populate with data
		//STEP2-1: declare variables with data
		idTH.setAttribute("scope", "row");
		let idText = document.createTextNode(currentReimb.reimbId);
		let amountD = document.createTextNode(currentReimb.amount);
		let typeD = document.createTextNode(currentReimb.type);
		let descrD = document.createTextNode(currentReimb.description);
		let statusD=document.createTextNode(currentReimb.status);
		let submitted= new Date(currentReimb.submitted);
		let submitDateD = document.createTextNode(submitted.toUTCString());
		let authorD = document.createTextNode(currentReimb.author);
		let resolved = "-";
		if(currentReimb.resolved!=null)
		{
			resolved = new Date(currentReimb.resolved).toUTCString();
		}
		
		let resolveDateD = document.createTextNode(resolved);
		let resolver=currentReimb.resolver;
		if(currentReimb.resolver=="null null"){
			resolver="-"
		}
		let resolverD = document.createTextNode(resolver);
		let receiptD = document.createTextNode(currentReimb.receipt);
		
		//STEP2-2: append all elements
		idTH.appendChild(idText);
		amountTD.appendChild(amountD);
		typeTD.appendChild(typeD);
		descrTD.appendChild(descrD);
		statusTD.appendChild(statusD);
		submitDateTD.appendChild(submitDateD);
		authorTD.appendChild(authorD);
		resolveDateTD.appendChild(resolveDateD);
		resolverTD.appendChild(resolverD);
		receiptTD.appendChild(receiptD);
		
		newTR.appendChild(idTH);
		newTR.appendChild(amountTD);
		newTR.appendChild(typeTD);
		newTR.appendChild(descrTD);
		newTR.appendChild(statusTD);
		newTR.appendChild(submitDateTD);
		newTR.appendChild(authorTD);
		newTR.appendChild(resolveDateTD);
		newTR.appendChild(resolverTD);
		newTR.appendChild(receiptTD);
		
		let newSelection = document.querySelector("#reimbursementTable-body");
		newSelection.appendChild(newTR);
	}
	
}