------------------------------------
--DROP TABLES
------------------------------------
DROP VIEW user_table_view;
DROP VIEW reimb_table_view;
DROP TABLE ers_reimbursement;
DROP TABLE ers_users;
DROP TABLE ers_reimbursement_status;
DROP TABLE ers_reimbursement_type;
DROP TABLE ers_user_roles;

------------------------------------
--CREATE TABLES
------------------------------------
CREATE TABLE ers_reimbursement_type(
reimb_type_id serial PRIMARY KEY
, reimb_type varchar(10) NOT null
);

CREATE TABLE ers_reimbursement_status(
reimb_status_id serial PRIMARY KEY
, reimb_status varchar(10) NOT null
);

CREATE TABLE ers_user_roles(
ers_user_role_id serial PRIMARY KEY
, user_role varchar(25) NOT null
);

CREATE TABLE ers_users(
ers_users_id serial PRIMARY key
, ers_username varchar(50) UNIQUE NOT null
, ers_password varchar(50) NOT null
, user_first_name varchar(100) NOT null
, user_last_name varchar(100) NOT null
, user_email varchar(150) UNIQUE NOT null
, user_role_id integer NOT null
, CONSTRAINT user_roles_fk
FOREIGN KEY (user_role_id) REFERENCES ers_user_roles (ers_user_role_id)
ON DELETE cascade
);

CREATE INDEX ers_users_unv1 ON ers_users (ers_username, user_email);


CREATE TABLE ers_reimbursement(
reimb_id serial PRIMARY KEY
, reimb_amount NUMERIC NOT null
, reimb_submitted timestamptz NOT null
, reimb_resolved timestamptz
, reimb_description varchar(250)
, reimb_reciept varchar(100)
, reimb_author integer NOT NULL
, reimb_resolver integer 
, reimb_status_id integer NOT null
, reimb_type_id integer NOT null
, CONSTRAINT ers_users_fk_auth 
FOREIGN KEY (reimb_author) REFERENCES ers_users (ers_users_id)
ON DELETE cascade
, CONSTRAINT ers_users_fk_reslvr
FOREIGN KEY (reimb_resolver) REFERENCES ers_users (ers_users_id)
ON DELETE cascade
, CONSTRAINT ers_reimbursement_status_fk
FOREIGN KEY (reimb_status_id) 
REFERENCES ers_reimbursement_status (reimb_status_id)
, CONSTRAINT ers_reimbursement_type_fk
FOREIGN KEY (reimb_type_id) REFERENCES ers_reimbursement_type (reimb_type_id)
);

------------------------------------
--INITIATE TABLES
------------------------------------

INSERT INTO ers_reimbursement_type (reimb_type) VALUES ('Lodging');
INSERT INTO ers_reimbursement_type (reimb_type) VALUES ('Travel');
INSERT INTO ers_reimbursement_type (reimb_type) VALUES ('Food');
INSERT INTO ers_reimbursement_type (reimb_type) VALUES ('Other');

INSERT INTO ers_user_roles (user_role) VALUES ('Employee');
INSERT INTO ers_user_roles (user_role) VALUES ('Finance Manager');
INSERT INTO ers_user_roles (user_role) VALUES ('admin');

INSERT INTO ers_reimbursement_status (reimb_status) VALUES ('Pending');
INSERT INTO ers_reimbursement_status (reimb_status) VALUES ('Approved');
INSERT INTO ers_reimbursement_status (reimb_status) VALUES ('Denied');

INSERT INTO ers_users (ers_username, ers_password, user_first_name 
			, user_last_name, user_email, user_role_id)
VALUES ('admin1', 'admin1', 'admin1_first', 'admin1_last', 'admin1@email.com', 3);

INSERT INTO ers_users (ers_username, ers_password, user_first_name 
			, user_last_name, user_email, user_role_id)
VALUES ('admin2', 'admin2', 'admin2_first', 'admin2_last', 'admin2@email.com', 2);

INSERT INTO ers_users (ers_username, ers_password, user_first_name 
			, user_last_name, user_email, user_role_id)
VALUES ('daBoss', 'password', 'Rufus', 'Shinra', 'rShinra@shinra.com', 2);

INSERT INTO ers_users (ers_username, ers_password, user_first_name 
			, user_last_name, user_email, user_role_id)
VALUES ('ancientHunter', 'ancients', 'Tseng', 'Turk', 'turkTseng@shinra.com', 1);

INSERT INTO ers_users (ers_username, ers_password, user_first_name 
			, user_last_name, user_email, user_role_id)
VALUES ('SOLDIER', 'PunyMortals7', 'Sephiroth', 'Hojo', 'sephiroth@shinra.com', 1);

INSERT INTO ers_users (ers_username, ers_password, user_first_name 
			, user_last_name, user_email, user_role_id)
VALUES ('madScientist', '12345', 'Professor', 'Hojo', 'madDoc@shinra.com', 1);

INSERT INTO ers_reimbursement (reimb_amount, reimb_submitted, reimb_description
, reimb_author, reimb_status_id, reimb_type_id)
VALUES (1000.00, current_timestamp,'5 star Hotel', 6, 1, 1);

INSERT INTO ers_reimbursement (reimb_amount, reimb_submitted, reimb_description
, reimb_author, reimb_status_id, reimb_type_id)
VALUES (100000.00, current_timestamp,'Life Insurance Payout', 5, 1, 4);

INSERT INTO ers_reimbursement (reimb_amount, reimb_submitted, reimb_resolved
, reimb_description, reimb_author, reimb_resolver, reimb_status_id, reimb_type_id)
VALUES (500.00, '2021-09-25 16:00:00',current_timestamp,'Helicopter Taxi Service', 4, 3, 2, 2);


------------------------------------
--CREATE VIEWS
------------------------------------
CREATE VIEW user_table_view AS 
SELECT * FROM ers_users eu INNER JOIN ers_user_roles eur 
ON eu.user_role_id = eur.ers_user_role_id 
;


CREATE VIEW reimb_table_view AS
SELECT er.*
, eu.user_first_name AS author_firstname
, eu.user_last_name AS author_lastname
, eu2.user_first_name AS resolver_firstname
, eu2.user_last_name AS resolver_lastname
, ers.reimb_status AS current_status
, ert.reimb_type AS expense_type
FROM ers_reimbursement er 
JOIN ers_reimbursement_status ers 
ON ers.reimb_status_id = er.reimb_status_id 
JOIN ers_reimbursement_type ert 
ON ert.reimb_type_id = er.reimb_type_id
LEFT JOIN ers_users eu 
ON eu.ers_users_id = er.reimb_author
LEFT JOIN ers_users eu2 
ON eu2.ers_users_id = er.reimb_resolver 
;

------------------------------------
--SELECT TABLES
------------------------------------
SELECT * FROM ers_reimbursement_type;
SELECT * FROM ers_reimbursement_status ers ;
SELECT * FROM ers_reimbursement er ;
SELECT * FROM ers_user_roles eur ;
SELECT * FROM ers_users eu ;
SELECT * FROM user_table_view;
SELECT * FROM reimb_table_view ;




